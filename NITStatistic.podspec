#
# Be sure to run `pod lib lint NITStatistic.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'NITStatistic'
  s.version          = '0.1.1'
  s.summary          = 'Napoleon IT statistic library.'
  s.description      = <<-DESC
TODO: Add long description of the pod here.
                       DESC

  s.homepage         = 'https://github.com/keekerun/NITStatistic'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'Aleksey Gulyaev' => 'ag@napoleonit.ru' }
  s.source           = { :git => 'https://github.com/keekerun/NITStatistic.git', :tag => s.version.to_s }
  s.social_media_url = 'https://telegram.me/keekerun'

  s.ios.deployment_target = '8.0'

  s.source_files = 'Classes/**/*.{h,m}'
  
  s.public_header_files = 'Classes/*.h'
  s.prefix_header_file = 'Classes/PrefixHeader.pch'

  s.frameworks = 'UIKit', 'CoreLocation'
  s.library   = 'sqlite3'
  
end
