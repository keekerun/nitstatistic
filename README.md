# NITStatistic

## Installation

NITStatistic доступа в репозитории спецификаций [Napoleon IT](https://github.com/keekerun/NITSpec.git). Для установки добавте в Podfile следующие строки в начало файла:
```ruby
source 'https://github.com/keekerun/NITSpec.git'
source 'https://github.com/CocoaPods/Specs.git'
```
И соответствующий pod в секцию с перечислением подключаемых библиотек:

```ruby
pod "NITStatistic"
```

## Author

Алексеей Гуляев, ag@napoleonit.ru, [telegramm](https://telegram.me/keekerun)
