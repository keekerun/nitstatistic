//
//  NITStatisticTests.m
//  NITStatisticTests
//
//  Created by Алексей Гуляев on 30.08.16.
//  Copyright © 2016 Napoleon It. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "NITStatisticManager.h"

@interface NITStatisticTests : XCTestCase

@end

@implementation NITStatisticTests

- (void)setUp {
    [super setUp];
}

- (void)tearDown {
    [super tearDown];
}

- (void)testExample {
    [NITStatisticManager startedWork];
    [NITStatisticManager addEvent:@"openWelcomScreen" data:@{@"fdsfsd":@"ffff"}];
}

- (void)testPerformanceExample {
    // This is an example of a performance test case.
    [self measureBlock:^{
        // Put the code you want to measure the time of here.
    }];
}

@end
