//
//  NITStatistic.h
//  NITStatistic
//
//  Created by Алексей Гуляев on 30.08.16.
//  Copyright © 2016 Napoleon It. All rights reserved.
//

#import <Foundation/Foundation.h>

//! Project version number for NITStatistic.
FOUNDATION_EXPORT double NITStatisticVersionNumber;

//! Project version string for NITStatistic.
FOUNDATION_EXPORT const unsigned char NITStatisticVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <NITStatistic/PublicHeader.h>

#ifndef _NITSTATISTIC_
#define _NITSTATISTIC_

#if TARGET_OS_IOS
#import <NITStatistic/NITStatisticManager.h>
#endif

#endif /* _NITSTATISTIC_ */