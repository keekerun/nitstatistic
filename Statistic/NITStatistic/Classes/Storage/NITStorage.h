//
//  NITStorage.h
//  NITStatistic
//
//  Created by Алексей Гуляев on 30.08.16.
//  Copyright © 2016 Napoleon It. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DataBase.h"

@interface NITStorage : NSObject

@property(nonatomic,strong) NSString *deviceId;
@property(nonatomic,strong) NSString *statisticServerURL;
@property(nonatomic) NSTimeInterval lastLocationsSend;

@property(nonatomic,readonly) DataBase *dataBase;

+ (instancetype)sharedManager;

@end
