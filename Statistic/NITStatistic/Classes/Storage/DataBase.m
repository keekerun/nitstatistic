//
//  DataBase.m
//  Unilever
//
//  Created by Алексей Гуляев on 29.12.15.
//  Copyright © 2015 Unilever. All rights reserved.
//

#import "DataBase.h"
#import <sqlite3.h>


@interface DataBase()
{
    NSString *_fileName;
    NSString *_pathToDbDirectory;
    
    sqlite3 *dataBase;
    dispatch_queue_t _queueBackground;
    dispatch_queue_t _queueResult;
}

@property(nonatomic, readonly) NSString *pathToDb;

@end


@implementation DataBase


#pragma mark - Lifecycle

- (instancetype)initWithPath:(NSString*)path fileName:(NSString*)fileName delegate:(nullable id<DataBaseDelegate>)delegate
{
    self = [super init];
    
    if(self)
    {
        _pathToDbDirectory = path;
        _fileName = fileName;
        _delegate = delegate;
        
        [self prepareDataBase];
    }
    
    return self;
}


#pragma mark - Custom Accessors

- (NSString *)pathToDb
{
    return _fileName ? [_pathToDbDirectory stringByAppendingPathComponent:_fileName] : nil;
}

- (void)setQueueBackground:(dispatch_queue_t)queueBackground
{
    _queueBackground = queueBackground;
}

- (dispatch_queue_t)queueBackground
{
    return _queueBackground ?: dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
}

- (void)setQueueResult:(dispatch_queue_t)queueResult
{
    _queueResult = queueResult;
}

- (dispatch_queue_t)queueResult
{
    return _queueResult ?: dispatch_get_main_queue();
}

#pragma mark - Public

- (NSArray*)queryString:(NSString*)query
{
    sqlite3_stmt *read_statement;
    const char *sql = [query UTF8String];
    
    if (sqlite3_prepare_v2(dataBase, sql, -1, &read_statement, NULL) != SQLITE_OK)
    {
        NSLog(@"Sqlite error %i:  %s", sqlite3_prepare_v2(dataBase, sql, -1, &read_statement, NULL), sqlite3_errmsg(dataBase));
        return nil;
    }
    
    NSMutableArray *rows = [NSMutableArray new];
    
    while (sqlite3_step(read_statement) == SQLITE_ROW)
    {
        NSMutableDictionary *row = [NSMutableDictionary new];
        
        for (int i = 0; i < sqlite3_column_count(read_statement); i++)
        {
            int type = sqlite3_column_type(read_statement, i);
            id value;
            NSString *key = [NSString stringWithUTF8String:sqlite3_column_name(read_statement, i)];
            
            switch (type)
            {
                case SQLITE_TEXT:
                    value = [NSString stringWithUTF8String:(char *)sqlite3_column_text(read_statement, i)];
                    break;
                    
                case SQLITE_INTEGER:
                    value = [NSNumber numberWithLongLong:sqlite3_column_int64(read_statement, i)];
                    break;
                    
                case SQLITE_FLOAT:
                    value = [NSNumber numberWithDouble:sqlite3_column_double(read_statement, i)];;
                    break;
                    
                case SQLITE_BLOB:
                    value = [[NSData alloc] initWithBytes:sqlite3_column_blob(read_statement, i)
                                                       length: sqlite3_column_bytes(read_statement, i)];
                    break;
                    
                default:
                    break;
            }
            
            if (value)
            {
                [row setValue:value forKey:key];
            }
        }
        
        [rows addObject:row];
    }
    
    sqlite3_reset(read_statement);
    
    return [NSArray arrayWithArray:rows];
    
}

- (void)queryStringAsync:(NSString*)query callback:(DBCallback)callback
{
    dispatch_async(self.queueBackground, ^{
        NSArray *result = [self queryString:query];
        dispatch_async(self.queueResult, ^{
            if (callback)
            {
                callback(result);
            }
        });
    });
}

- (NSArray *)getListTable:(NSString *)table where:(NSDictionary *)where
{
    return [self getListTable:table where:where count:-1 offset:-1];
}

- (void)getListTableAsync:(NSString*)table where:(NSDictionary*)where callback:(DBCallback)callback
{
    [self getListTableAsync:table where:where count:-1 offset:-1 callback:callback];
}

- (NSArray *)getListTable:(NSString *)table where:(NSDictionary *)where count:(NSInteger)count offset:(NSInteger)offset
{
    return [self getListTable:table where:where count:count offset:offset isOR:NO];
}

- (void)getListTableAsync:(NSString *)table where:(NSDictionary *)where count:(NSInteger)count offset:(NSInteger)offset callback:(DBCallback)callback
{
    [self getListTableAsync:table where:where count:count offset:offset isOR:NO callback:callback];
}

- (NSArray*)getListTable:(NSString*)table where:(NSDictionary*)where count:(NSInteger)count offset:(NSInteger)offset isOR:(BOOL)isOR
{
    NSString *query = [NSString stringWithFormat:@"SELECT * FROM \"%@\"", table];
    
    NSString *whereString = [self whereWithDictionary:where];
    
    if (whereString.length > 0)
    {
        query = [NSString stringWithFormat:@"%@ WHERE %@", query, whereString];
    }
    
    if (count > 0)
    {
        query = [NSString stringWithFormat:@"%@ LIMIT %lu", query, (unsigned long)count];
    }
    
    if (offset > 0)
    {
        query = [NSString stringWithFormat:@"%@ OFFSET %lu", query, (unsigned long)offset];
    }
    
    return [self queryString:query];
    
}

- (void)getListTableAsync:(NSString*)table where:(NSDictionary*)where count:(NSInteger)count offset:(NSInteger)offset isOR:(BOOL)isOR callback:(DBCallback)callback
{
    dispatch_async(self.queueBackground, ^{
        NSArray *result = [self getListTable:table where:where count:count offset:offset isOR:isOR];
        dispatch_async(self.queueResult, ^{
            if (callback)
            {
                callback(result);
            }
        });
    });
}

- (NSUInteger)insertToTable:(NSString *)table data:(NSDictionary *)data
{
    sqlite3_stmt *insert_statement;
    
    NSArray *keys = [data allKeys];
    NSMutableArray *values = [NSMutableArray new];
    
    for (NSUInteger i = 0; i < keys.count; i++)
    {
        [values addObject:@"?"];
    }
    
    NSString *sql = [NSString stringWithFormat:@"INSERT INTO \"%@\"(%@) VALUES(%@);", table, [keys componentsJoinedByString:@", "], [values componentsJoinedByString:@", "]];
    const char *sqlChar = [sql UTF8String];
    
    if (sqlite3_prepare_v2(dataBase, sqlChar, -1, &insert_statement, NULL) != SQLITE_OK)
    {
        NSLog(@"SQL ERROR: CONSTANT = %i %s", sqlite3_prepare_v2(dataBase, sqlChar, -1, &insert_statement, NULL), sqlite3_errmsg(dataBase));
        return 0;
    }
    
    int index = 1;
    for (NSString *key in keys)
    {
        id value = data[key];
        
        if ( [value isKindOfClass:[NSNumber class]] )
        {
            NSNumber *number = (NSNumber *)value;
            if( [self numberIsFraction:number] )
            {
                sqlite3_bind_double(insert_statement, index, [number doubleValue]);
            }
            else
            {
                sqlite3_bind_int(insert_statement, index, [number intValue]);
            }
        }
        else if ( [value isKindOfClass:[NSString class]] )
        {
            NSString *string = (NSString *)value;
            sqlite3_bind_text(insert_statement, index, [string UTF8String], -1, SQLITE_TRANSIENT);
        }
        else if( [value isKindOfClass:[NSData class]] )
        {
            NSData *data = (NSData *)value;
            sqlite3_bind_blob(insert_statement, index, [data bytes], (int)data.length, SQLITE_TRANSIENT);
        }
        else
        {
            NSAssert(NO, @"Error class type");
        }
        
        index++;
    }
    
    if (sqlite3_step(insert_statement) != SQLITE_DONE)
    {
        sqlite3_reset(insert_statement);
        return 0;
    }
    else
    {
        long long insertId = sqlite3_last_insert_rowid(dataBase);
        sqlite3_reset(insert_statement);
        return (NSUInteger)insertId;
    }
}

- (void)insertToTableAsync:(NSString *)table data:(NSDictionary *)data callback:(DBCallback)callback
{
    dispatch_async(self.queueBackground, ^{
        NSUInteger result = [self insertToTable:table data:data];
        dispatch_async(self.queueResult, ^{
            if (callback)
            {
                callback(@(result));
            }
        });
    });
}

- (BOOL)updateTable:(NSString *)table data:(NSDictionary *)data where:(NSString *)where
{
    sqlite3_stmt *update_statement;
    
    NSString *preSql = [NSString stringWithFormat:@"UPDATE \"%@\" SET ", table];
    for (NSString *key in data)
    {
        preSql = [NSString stringWithFormat:@"%@\"%@\"=?, ", preSql, key];
    }
    
    preSql = [preSql substringToIndex:preSql.length - 2];
    
    if ( [where isKindOfClass:[NSString class]] && [where length] > 0 )
    {
        preSql = [NSString stringWithFormat:@"%@ WHERE %@;", preSql, where];
    }
    
    const char *sql = [preSql UTF8String];
    
    if (sqlite3_prepare_v2(dataBase, sql, -1, &update_statement, NULL) != SQLITE_OK)
    {
        NSLog(@"SQL ERROR: CONSTANT = %i %s", sqlite3_prepare_v2(dataBase, sql, -1, &update_statement, NULL), sqlite3_errmsg(dataBase));
        return NO;
    }
    
    int index = 1;
    for (NSString *key in data)
    {
        id value = data[key];
        
        if ( [value isKindOfClass:[NSNumber class]] )
        {
            NSNumber *number = (NSNumber *)value;
            
            if( [self numberIsFraction:number] )
            {
                sqlite3_bind_double(update_statement, index, [number doubleValue]);
            }
            else
            {
                sqlite3_bind_int(update_statement, index, [number intValue]);
            }
        }
        else if ( [value isKindOfClass:[NSString class]] )
        {
            NSString *string = (NSString *)value;
            sqlite3_bind_text(update_statement, index, [string UTF8String], -1, SQLITE_TRANSIENT);
        }
        else if( [value isKindOfClass:[NSData class]] )
        {
            NSData *data = (NSData *)value;
            sqlite3_bind_blob(update_statement, index, [data bytes], (int)data.length, SQLITE_TRANSIENT);
        }
        index++;
    }
    
    if (sqlite3_step(update_statement) != SQLITE_DONE)
    {
        sqlite3_reset(update_statement);
        return NO;
    }
    else
    {
        sqlite3_reset(update_statement);
        return YES;
    }
}

- (void)updateTableAsync:(NSString *)table data:(NSDictionary *)data where:(NSString *)where callback:(DBCallback)callback
{
    dispatch_async(self.queueBackground, ^{
        BOOL result = [self updateTable:table data:data where:where];
        dispatch_async(self.queueResult, ^{
            if (callback)
            {
                callback(@(result));
            }
        });
    });
}

- (BOOL)deleteFromTable:(NSString *)table where:(NSString *)where
{
    sqlite3_stmt *delete_statement;
    
    NSString *preSql = [NSString stringWithFormat:@"DELETE FROM \"%@\" WHERE %@;", table, where];
    const char *sql = [preSql UTF8String];
    
    if (sqlite3_prepare_v2(dataBase, sql, -1, &delete_statement, NULL) != SQLITE_OK)
    {
        return NO;
    }
    
    if (sqlite3_step(delete_statement) != SQLITE_DONE)
    {
        sqlite3_reset(delete_statement);
        return NO;
    }
    else
    {
        sqlite3_reset(delete_statement);
        return YES;
    }
}

- (void)deleteFromTableAsync:(NSString *)table where:(NSString *)where callback:(DBCallback)callback
{
    dispatch_async(self.queueBackground, ^{
        BOOL result = [self deleteFromTable:table where:where];
        dispatch_async(self.queueResult, ^{
            if (callback)
            {
                callback(@(result));
            }
        });
    });
}

- (BOOL)saveDataToTable:(NSString *)table data:(NSDictionary *)data where:(NSString *)where
{
    NSString* query = [[NSString alloc] initWithFormat:@"SELECT * FROM \"%@\" WHERE %@", table, where];
    
    if([self queryString:query].count != 0)
    {
        return [self updateTable:table data:data where:where];
    }
    else
    {
        return [self insertToTable:table data:data] > 0;
    }
}

- (void)saveDataToTableAsync:(NSString *)table data:(NSDictionary *)data where:(NSString *)where callback:(DBCallback)callback
{
    dispatch_async(self.queueBackground, ^{
        BOOL result = [self saveDataToTable:table data:data where:where];
        dispatch_async(self.queueResult, ^{
            if (callback)
            {
                callback(@(result));
            }
        });
    });
}

#pragma mark - Private

- (NSString*)whereWithDictionary:(NSDictionary*)where
{
    NSString *whereString = nil;
    
    if ([where isKindOfClass:[NSDictionary class]])
    {
        NSArray *keys = [where allKeys];
        NSMutableArray *param = [NSMutableArray new];
        
        for (NSString *key in keys)
        {
            id value = where[key];
            [param addObject:[self whereCondition:key value:value]];
        }
        
        if (param.count > 0)
        {
            whereString = [param componentsJoinedByString:@" AND "];
        }
    }
    else if ([where isKindOfClass:[NSString class]])
    {
        whereString = (NSString*)where;
    }
    
    return whereString;
}

- (NSString*)whereCondition:(NSString*)key value:(id)value
{
    if ([value isKindOfClass:[NSString class]])
    {
        return [NSString stringWithFormat:@"\"%@\" = \"%@\"", key, (NSString*)value];
    }
    else if ([value isKindOfClass:[NSNumber class]])
    {
        return [NSString stringWithFormat:@"\"%@\" = %@", key, [(NSNumber*)value stringValue]];
    }
    else if ([value isKindOfClass:[NSDictionary class]])
    {
        return [self whereWithDictionary:value];
    }
    else if ([value isKindOfClass:[NSArray class]])
    {
        NSMutableArray *param = [NSMutableArray new];
        for (id object in (NSArray*)value)
        {
            [param addObject:[self whereCondition:key value:object]];
        }
        
        return param.count > 0 ? [NSString stringWithFormat:@"(%@)", [param componentsJoinedByString:@" OR "]] : @"";
    }
    else
    {
        NSAssert(NO, @"Where value no support object type");
    }
    
    return nil;
}

- (BOOL)numberIsFraction:(NSNumber *)number
{
    double diff = number.doubleValue - number.intValue;
    return diff > 0;
}

#pragma mark - Prepare DataBase

- (void)prepareDataBase
{
    @synchronized (self)
    {
        NSError * error = nil;
        
        [[NSFileManager defaultManager] createDirectoryAtPath:_pathToDbDirectory
                                  withIntermediateDirectories:YES
                                                   attributes:nil
                                                        error:&error];
        
        if (error == nil)
        {
            BOOL isCreate = ![self isDBExists];
            
            
            int code = sqlite3_open_v2([self.pathToDb UTF8String], &dataBase, SQLITE_OPEN_READWRITE | SQLITE_OPEN_CREATE, nil);
            
            if (code != SQLITE_OK)
            {
                NSLog(@"Error create database");
            }
            else if (isCreate && self.delegate)
            {
                [self.delegate dataBaseCreate:self];
            }
        }
        else
        {
            NSLog(@"error creating directory: %@", error);
        }
    }
}

- (BOOL)isDBExists
{
    return [[NSFileManager defaultManager] fileExistsAtPath:self.pathToDb];
}

- (void)closeDatabase
{
    sqlite3_close(dataBase);
}

- (NSError*)deleteDataBase
{
    [self closeDatabase];
    
    NSFileManager *manager = [NSFileManager defaultManager];
    NSError *error;
    
    if ( [self isDBExists] )
    {
        [manager removeItemAtPath:self.pathToDb error:&error];
    }
    
    return error;
}

@end
