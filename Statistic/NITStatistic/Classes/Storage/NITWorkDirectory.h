//
//  NITWorkDirectory.h
//  NITStatistic
//
//  Created by Алексей Гуляев on 31.08.16.
//  Copyright © 2016 Napoleon It. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NITWorkDirectory : NSObject

+ (NSString*)workPath;

@end
