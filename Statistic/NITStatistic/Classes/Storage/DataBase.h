//
//  DataBase.h
//  Unilever
//
//  Created by Алексей Гуляев on 29.12.15.
//  Copyright © 2015 Unilever. All rights reserved.
//

#import <Foundation/Foundation.h>


@class DataBase;


/**
 *  Протокол оператора базы данных
 */
@protocol DataBaseDelegate <NSObject>

/**
 *  Событие создание базы данных. 
 *  В данном методе требуется создать все необходимые для работы базы данных таблицы.
 *
 *  @param database Объект оператора базы данных
 */
- (void)dataBaseCreate:(nonnull DataBase*)database;
@end


/**
 *  Тип блока асинхронных запросов к базе данных
 *
 *  @param id результата запроса
 */
typedef void (^DBCallback)(__nullable id);


/**
 *  Класс оператора базы данных
 */
@interface DataBase : NSObject

@property(nonatomic, weak) _Nullable id<DataBaseDelegate> delegate;
@property(nonatomic) _Nullable dispatch_queue_t queueBackground;
@property(nonatomic) _Nullable dispatch_queue_t queueResult;

- (nullable instancetype)initWithPath:(nonnull NSString*)path
                             fileName:(nonnull NSString*)fileName
                             delegate:(nullable id<DataBaseDelegate>)delegate;

- (nullable NSArray*)queryString:(nonnull NSString*)query;

- (void)queryStringAsync:(nonnull NSString*)query
                callback:(nullable DBCallback)callback;

- (nullable NSArray*)getListTable:(nonnull NSString*)table
                            where:(nullable NSDictionary*)where;

- (void)getListTableAsync:(nonnull NSString*)table
                    where:(nullable NSDictionary*)where
                 callback:(nullable DBCallback)callback;


- (nullable NSArray*)getListTable:(nonnull NSString*)table
                            where:(nullable NSDictionary*)where
                            count:(NSInteger)count
                           offset:(NSInteger)offset;

- (void)getListTableAsync:(nonnull NSString *)table
                    where:(nullable NSDictionary *)where
                    count:(NSInteger)count
                   offset:(NSInteger)offset
                 callback:(nullable DBCallback)callback;

- (nullable NSArray*)getListTable:(nonnull NSString*)table
                            where:(nullable NSDictionary*)where
                            count:(NSInteger)count
                           offset:(NSInteger)offset
                             isOR:(BOOL)isOR;

- (void)getListTableAsync:(nonnull NSString*)table
                    where:(nullable NSDictionary*)where
                    count:(NSInteger)count
                   offset:(NSInteger)offset
                     isOR:(BOOL)isOR
                 callback:(nullable DBCallback)callback;

- (NSUInteger)insertToTable:(nonnull NSString *)table
                       data:(nonnull NSDictionary *)data;

- (void)insertToTableAsync:(nonnull NSString *)table
                      data:(nonnull NSDictionary *)data
                  callback:(nullable DBCallback)callback;

- (BOOL)updateTable:(nonnull NSString *)table
               data:(nonnull NSDictionary *)data
              where:(nonnull NSString *)where;

- (void)updateTableAsync:(nonnull NSString *)table
                    data:(nonnull NSDictionary *)data
                   where:(nonnull NSString *)where
                callback:(nullable DBCallback)callback;

- (BOOL)deleteFromTable:(nonnull NSString *)table
                  where:(nonnull NSString *)where;

- (void)deleteFromTableAsync:(nonnull NSString *)table
                       where:(nonnull NSString *)where
                    callback:(nullable DBCallback)callback;

- (BOOL)saveDataToTable:(nonnull NSString*)table
                   data:(nonnull NSDictionary*)data
                  where:(nonnull NSString*)where;

- (void)saveDataToTableAsync:(nonnull NSString *)table
                        data:(nonnull NSDictionary *)data
                       where:(nonnull NSString *)where
                    callback:(nullable DBCallback)callback;

@end
