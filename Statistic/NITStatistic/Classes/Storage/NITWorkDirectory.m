//
//  NITWorkDirectory.m
//  NITStatistic
//
//  Created by Алексей Гуляев on 31.08.16.
//  Copyright © 2016 Napoleon It. All rights reserved.
//

#import "NITWorkDirectory.h"

@implementation NITWorkDirectory

static NSString* const workDirectoryName = @"ru.napoleonit.statistic";
static NSString* workDirectoryPath;

+ (NSString*)workPath
{
    static dispatch_once_t once;
    
    dispatch_once(&once, ^{
        workDirectoryPath = [NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES) objectAtIndex:0];
        workDirectoryPath = [workDirectoryPath stringByAppendingPathComponent:workDirectoryName];
        
        NSError * error = nil;
        
        [[NSFileManager defaultManager] createDirectoryAtPath:workDirectoryPath
                                  withIntermediateDirectories:YES
                                                   attributes:nil
                                                        error:&error];
        
        if (error != nil)
        {
            NSLog(@"Error creating directory: %@", error);
        }
    });
    
    return workDirectoryPath;
}

@end
