//
//  NITStorage.m
//  NITStatistic
//
//  Created by Алексей Гуляев on 30.08.16.
//  Copyright © 2016 Napoleon It. All rights reserved.
//

#import "NITStorage.h"
#import "NITWorkDirectory.h"
#import "PDKeychainBindings.h"
#import "FBEncryptorAES.h"
#import "NITEvent.h"

@interface NITStorage() <DataBaseDelegate>
{
    NSMutableDictionary<NSString*,NSString*> *memoryCache;
    DataBase *_database;
}

@end


@implementation NITStorage


static NSString* const plistFileName = @"storage";
static NSString* const keyAESKeychain = @"NITStatistic.AESKey";
static NSString* const sqliteFileName = @"event.db";


#pragma mark - Lifecycle

- (instancetype)init
{
    NSAssert(NO, @"Using sharedManager");
    return nil;
}

- (instancetype)initPrivate
{
    self = [super init];
    
    if (self)
    {
        _database = [[DataBase alloc] initWithPath:[NITWorkDirectory workPath] fileName:sqliteFileName delegate:self];
    }
    
    return self;
}

+ (instancetype)sharedManager
{
    static dispatch_once_t once;
    static id sharedInstance;
    dispatch_once(&once, ^{
        sharedInstance = [[self alloc] initPrivate];
    });
    return sharedInstance;
}


#pragma mark - Custom Accessors

- (DataBase *)dataBase
{
    return _database;
}

- (void)setDeviceId:(NSString *)value
{
    [self setString:value forKey:@keypath1(self.deviceId)];
    [self setToKeychainString:value forKey:[self keyForKeychainWithString:@keypath1(self.deviceId)]];
}

- (NSString *)deviceId
{
    NSString *deviceId = [self stringFromKeychainForKey:[self keyForKeychainWithString:@keypath1(self.deviceId)]];
    
    if ([deviceId length] == 0)
    {
        @synchronized (self)
        {
            deviceId = [self stringFromKeychainForKey:[self keyForKeychainWithString:@keypath1(self.deviceId)]];
            
            if ([deviceId length] == 0)
            {
                deviceId = [self stringForKey:@keypath1(self.deviceId)];
                
                if ([deviceId length] == 0)
                {
                    deviceId = [[NSUUID UUID] UUIDString];
                    [self setDeviceId:deviceId];
                }
            }
        }
    }
    
    return deviceId;
}

- (void)setStatisticServerURL:(NSString*)statisticServerURL
{
    [self setString:statisticServerURL forKey:@keypath1(self.statisticServerURL)];
}

- (NSString*)statisticServerURL
{
    return [self stringForKey:@keypath1(self.statisticServerURL)];
}

- (void)setLastLocationsSend:(NSTimeInterval)lastLocationsSend
{
    [self setNumber:@(lastLocationsSend) forKey:@keypath1(self.lastLocationsSend)];
}

- (NSTimeInterval)lastLocationsSend
{
    return [[self numberForKey:@keypath1(self.lastLocationsSend)] doubleValue];
}

#pragma mark - Public

#pragma mark - Private

- (void)setNumber:(NSNumber*)value forKey:(NSString*)key
{
    if (!memoryCache)
    {
        [self loadData];
    }
    
    if ([value isKindOfClass:[NSNumber class]])
    {
        [memoryCache setObject:value forKey:key];
    }
    else if (memoryCache[key])
    {
        [memoryCache removeObjectForKey:key];
    }
    
    [self saveData];
}

- (NSNumber*)numberForKey:(NSString*)key
{
    if (!memoryCache)
    {
        [self loadData];
    }
    
    return memoryCache[key];
}

- (void)setString:(NSString*)value forKey:(NSString*)key
{
    if (!memoryCache)
    {
        [self loadData];
    }
    
    if ([value isKindOfClass:[NSString class]])
    {
        value = [FBEncryptorAES encryptBase64String:value keyString:[self keyForAES] separateLines:NO];
        [memoryCache setObject:value forKey:key];
    }
    else if (memoryCache[key])
    {
        [memoryCache removeObjectForKey:key];
    }
    
    [self saveData];
}

- (NSString*)stringForKey:(NSString*)key
{
    if (!memoryCache)
    {
        [self loadData];
    }
 
    NSString *cryptString = memoryCache[key];
    
    return [cryptString isKindOfClass:[NSString class]] ? [FBEncryptorAES decryptBase64String:cryptString keyString:[self keyForAES]] : nil;
}

- (void)setToKeychainString:(NSString*)value forKey:(NSString*)key
{
    PDKeychainBindings *bindings = [PDKeychainBindings sharedKeychainBindings];
    [bindings setString:value forKey:key];
}

- (NSString*)stringFromKeychainForKey:(NSString*)key
{
    PDKeychainBindings *bindings = [PDKeychainBindings sharedKeychainBindings];
    return [bindings stringForKey:key];
}

- (NSString*)keyForKeychainWithString:(NSString*)key
{
    return [[[NSBundle mainBundle] bundleIdentifier] stringByAppendingFormat:@".%@", key];
}

- (void)saveData
{
    if (memoryCache)
    {
        [memoryCache writeToFile:@"" atomically:YES];
    }
    
    [memoryCache writeToFile:[self filePath] atomically:YES];
}

- (void)loadData
{
    memoryCache = [[NSDictionary dictionaryWithContentsOfFile:[self filePath]] mutableCopy];
    
    if (![memoryCache isKindOfClass:[NSMutableDictionary class]])
    {
        memoryCache = [NSMutableDictionary new];
    }
}

- (NSString*)filePath
{
    return [[NITWorkDirectory workPath] stringByAppendingPathComponent:plistFileName];
}

- (NSString*)keyForAES
{
    NSString *key = [self keyForKeychainWithString:keyAESKeychain];
    NSString *aes = [self stringFromKeychainForKey:key];
    
    if ([aes length] == 0)
    {
        aes = [[NSUUID UUID] UUIDString];
        [self setToKeychainString:aes forKey:key];
    }
    
    return key;
}


#pragma mark - DataBaseDelegate

- (void)dataBaseCreate:(DataBase *)database
{
    [NITEvent createTableWithDatabase:database];
}

@end












