//
//  NITStatisticManager.m
//  NITStatistic
//
//  Created by Алексей Гуляев on 30.08.16.
//  Copyright © 2016 Napoleon It. All rights reserved.
//

#import "NITStatisticManager.h"
#import "NITLogWriter.h"
#import "NITLogHandler.h"
#import "NITStorage.h"
#import "NITLogObserver.h"

@implementation NITStatisticManager


#pragma mark - Lifecycle

- (instancetype)init
{
    NSAssert(NO, @"Not static method");
    return nil;
}


#pragma mark - Public

+ (void)startedWithURLServer:(nonnull NSString*)urlServer
{
    [NITStorage sharedManager].statisticServerURL = urlServer;
    [[NITLogHandler sharedManager].observer notificationDidBecomeActive:nil];
}

+ (void)addEvent:(NSString *)eventKey
{
    [self addEvent:eventKey data:nil];
}

+ (void)addEvent:(NSString *)eventKey data:(NSDictionary<NSString*, NSObject*>*)data
{
    [NITLogWriter addEventWichMethod:NITEventMethodEvent eventKey:eventKey data:data];
}

+ (void)setUserId:(NSString *)userId
{
    [NITLogWriter addEventWichMethod:NITEventMethodSetUserId eventKey:nil data:@{@"value":userId}];
}

@end
