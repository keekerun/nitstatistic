//
//  NITStatisticManager+Private.m
//  NITStatistic
//
//  Created by Алексей Гуляев on 08.09.16.
//  Copyright © 2016 Napoleon It. All rights reserved.
//

#import "NITStatisticManager+Private.h"
#import <CoreLocation/CoreLocation.h>
#import "NITLogWriter.h"

@implementation NITStatisticManager (Private)

#pragma mark - Public

+ (void)setLocation:(CLLocation*)location
{
    NSAssert([location isKindOfClass:[CLLocation class]], @"Object is not class CLLocation");
    
    NSDictionary *data = @{
                           @"value":@{
                                   @"lat":@(location.coordinate.latitude),
                                   @"lon":@(location.coordinate.longitude)
                                   }
                           };
    
    [NITLogWriter addEventWichMethod:NITEventMethodSetLocation eventKey:nil data:data];
}

+ (void)sessionStart:(NSDictionary*)data
{
    [NITLogWriter addEventWichMethod:NITEventMethodSessionStart eventKey:nil data:data];
}

+ (void)sessionEnd:(NSDictionary*)data
{
    [NITLogWriter addEventWichMethod:NITEventMethodSessionEnd eventKey:nil data:data];
}

#pragma mark - Private

@end
