//
//  NITLogHandler.h
//  NITStatistic
//
//  Created by Алексей Гуляев on 07.09.16.
//  Copyright © 2016 Napoleon It. All rights reserved.
//

#import <Foundation/Foundation.h>

@class NITLogObserver;

@interface NITLogHandler : NSObject

@property(nonatomic,readonly) NITLogObserver *observer;
@property(nonatomic, readonly) NSOperationQueue *queueWrite;

+ (instancetype)sharedManager;

@end
