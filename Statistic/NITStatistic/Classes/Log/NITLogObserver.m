//
//  NITLogObserver.m
//  NITStatistic
//
//  Created by Алексей Гуляев on 08.09.16.
//  Copyright © 2016 Napoleon It. All rights reserved.
//

#import "NITLogObserver.h"
#import <sys/utsname.h>
#import <UIKit/UIKit.h>
#import "NITStatisticManager+Private.h"
#import <CoreLocation/CoreLocation.h>
#import "NITStorage.h"

@interface NITLogObserver() <CLLocationManagerDelegate>
{
    CLLocationManager *_locationManager;
}

@property(nonatomic) BOOL deferringUpdates;

@end

@implementation NITLogObserver

#pragma mark - Lifecycle

- (instancetype)init
{
    self = [super init];
    
    if (self)
    {
        [self startSessionObserving];
        [self startLocationObserving];
    }
    
    return self;
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - Session

- (void)startSessionObserving
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(notificationDidBecomeActive:)
                                                 name:UIApplicationWillEnterForegroundNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(notificationDidEnterBackground:)
                                                 name:UIApplicationDidEnterBackgroundNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(notificationDidEnterBackground:)
                                                 name:UIApplicationWillTerminateNotification
                                               object:nil];
}

- (void)notificationDidBecomeActive:(NSNotification*)notification
{
    struct utsname systemInfo;
    uname(&systemInfo);
    
    NSString *os = @"iOS";
    NSString *version = [[UIDevice currentDevice] systemVersion];
    NSString *device = [NSString stringWithCString:systemInfo.machine
                                          encoding:NSUTF8StringEncoding];
    
    NSDictionary *data = @{
                           @"device": device,
                           @"os": @{
                                   @"platform": os,
                                   @"version": version,
                                   }
                           };
    
    [NITStatisticManager sessionStart:data];
}

- (void)notificationDidEnterBackground:(NSNotification*)notification
{
    [NITStatisticManager sessionEnd:nil];
}

#pragma mark - Location

- (void)startLocationObserving
{
    if ([CLLocationManager locationServicesEnabled])
    {
        _locationManager = [CLLocationManager new];
        _locationManager.delegate = self;
        _locationManager.distanceFilter = kCLLocationAccuracyThreeKilometers;
    }
}

- (void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status
{
    if (status == kCLAuthorizationStatusAuthorizedAlways || status == kCLAuthorizationStatusAuthorizedWhenInUse)
    {
        [manager startUpdatingLocation];
    }
    else
    {
        [manager stopUpdatingLocation];
    }
}

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(nonnull NSArray<CLLocation *> *)locations
{
    if ([NSDate date].timeIntervalSince1970 - [NITStorage sharedManager].lastLocationsSend > 3600)
    {
        if ([locations lastObject])
        {
            [NITStatisticManager setLocation:[locations lastObject]];
            [NITStorage sharedManager].lastLocationsSend = [NSDate date].timeIntervalSince1970;
        }
        
        if (!self.deferringUpdates && [CLLocationManager deferredLocationUpdatesAvailable])
        {
            [_locationManager allowDeferredLocationUpdatesUntilTraveled:kCLLocationAccuracyThreeKilometers timeout:1800];
            self.deferringUpdates = YES;
        }
    }
}

-(void)locationManager:(CLLocationManager *)manager didFinishDeferredUpdatesWithError:(NSError *)error
{
    self.deferringUpdates = NO;
}

#pragma mark - Private


@end
