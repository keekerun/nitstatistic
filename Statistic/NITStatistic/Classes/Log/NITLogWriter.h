//
//  NITLogWriter.h
//  NITStatistic
//
//  Created by Алексей Гуляев on 07.09.16.
//  Copyright © 2016 Napoleon It. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NITEvent.h"
#import "NITStorage.h"

@interface NITLogWriter : NSObject

+ (void)addEventWichMethod:(NITEventMethod)method eventKey:(NSString*)eventKey data:(NSDictionary*)data;

@end
