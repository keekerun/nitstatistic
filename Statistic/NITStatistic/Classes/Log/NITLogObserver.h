//
//  NITLogObserver.h
//  NITStatistic
//
//  Created by Алексей Гуляев on 08.09.16.
//  Copyright © 2016 Napoleon It. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NITLogObserver : NSObject

- (void)notificationDidBecomeActive:(NSNotification*)notification;

@end
