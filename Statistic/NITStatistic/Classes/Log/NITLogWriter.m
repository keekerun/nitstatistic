//
//  NITLogWriter.m
//  NITStatistic
//
//  Created by Алексей Гуляев on 07.09.16.
//  Copyright © 2016 Napoleon It. All rights reserved.
//

#import "NITLogWriter.h"
#import "NITLogHandler.h"
#import "NITStorage.h"

@implementation NITLogWriter


#pragma mark - Lifecycle


#pragma mark - Custom Accessors


#pragma mark - Public

+ (void)addEventWichMethod:(NITEventMethod)method eventKey:(NSString*)eventKey data:(NSDictionary*)data
{
    [[NITLogHandler sharedManager].queueWrite addOperationWithBlock:^{
        NITEvent *event = [NITEvent new];
        event.database = [NITStorage sharedManager].dataBase;
        event.method = method;
        event.event = eventKey;
        event.timestamp = [[NSDate date] timeIntervalSince1970];
        event.device = [NITStorage sharedManager].deviceId;
        event.data = data;
        
        [event saveDB];
    }];
}


#pragma mark - Private

@end
