//
//  NITLogOperationSender.m
//  NITStatistic
//
//  Created by Алексей Гуляев on 10.09.16.
//  Copyright © 2016 Napoleon It. All rights reserved.
//

#import "NITLogOperationSender.h"
#import "NITEvent.h"
#import "NITStorage.h"
#import "NSArray+BlocksKit.h"
#import "NITBatch.h"

@interface NITLogOperationSender()
{
    __unused NITEvent *_event;
    
    dispatch_semaphore_t semaphoreSending;
}

@end

@implementation NITLogOperationSender

#pragma mark - Lifecycle

- (void)main
{
    //@try {
        semaphoreSending = dispatch_semaphore_create(0);
        NSArray *events = [self readEvents];
        
        if ([events count] > 0)
        {
            [self sendEvents:events];
            [self removeOnlyCompleteEvents:events];
        }
    //}
    /*@catch (NSException *exception) {
        NSLog(@"Exeption NITLogOperationSender: %@", exception.description);
        dispatch_semaphore_signal(semaphoreSending);
    };*/
}

#pragma mark - Custom Accessors

#pragma mark - Private

- (NSArray<NITEvent*>*)readEvents
{
    NSString *sqlFormat = @"SELECT * FROM \"%@\" WHERE \"%@\" = %lu ORDER BY \"%@\" ASC LIMIT %d";
    NSString *orderRow = @keypath1(_event.timestamp);
    int limit = 50;
    
    NSString *sql = [NSString stringWithFormat: sqlFormat,
                                                [NITEvent tableDB],
                                                @keypath1(_event.status),
                                                (unsigned long)NITEventStatusNew,
                                                orderRow,
                                                limit];
    
    DataBase *database = [NITStorage sharedManager].dataBase;
    
    return [[database queryString:sql] bk_mapNotNil:^id(NSDictionary *obj) {
        return [[NITEvent alloc] initWithAttribute:obj database:database];
    }];
}

- (void)sendEvents:(NSArray<NITEvent*>*)events
{
    NSString *stringURL = [NITStorage sharedManager].statisticServerURL;
    
    if ([stringURL length] == 0)
    {
        NSLog(@"Need set statistic server URL");
        return;
    }
    
    // Помечаем события как отправляемые в данный момент
    [[NITStorage sharedManager].dataBase updateTable:[NITEvent tableDB]
                                                data:@{@keypath1(_event.status): @(NITEventStatusSending)}
                                               where:[self whereConditionORForEvents:events]];
    
    NSURL *url = [NSURL URLWithString:stringURL];
    NSURLSessionConfiguration *config = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:config];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:url];
    request.HTTPMethod = @"POST";
    
    NITBatch *batch = [[NITBatch alloc] initWithEvents:events];
    NSDictionary *dictionary = [batch dictionary];
    
    NSError *error = nil;
    
    NSData *data = [NSJSONSerialization dataWithJSONObject:dictionary
                                                   options:kNilOptions error:&error];
    
    if (!error)
    {
        NSURLSessionUploadTask *uploadTask;
        
        uploadTask = [session uploadTaskWithRequest:request
                                           fromData:data
                                  completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                      
                                      if (error)
                                      {
                                          NSLog(@"Error send statistic: %@", error.description);
                                      }
                                      
                                      dispatch_semaphore_signal(semaphoreSending);
                                  }];
        
        [uploadTask resume];
        
        dispatch_semaphore_wait(semaphoreSending, DISPATCH_TIME_FOREVER);
    }
    else
    {
        // Помечаем как с ошибкой
        [[NITStorage sharedManager].dataBase updateTable:[NITEvent tableDB]
                                                    data:@{@keypath1(_event.status): @(NITEventStatusError)}
                                                   where:[self whereConditionORForEvents:events]];
        
        NSLog(@"Error serilizations statistic data: %@", error.description);
    }
}

- (void)removeOnlyCompleteEvents:(NSArray<NITEvent*>*)events
{
    NSString *whereFormat = @"(%@) AND \"%@\" == %lu";
    NSString *where = [NSString stringWithFormat:whereFormat, [self whereConditionORForEvents:events], @keypath1(_event.status), NITEventStatusSending];
    [[NITStorage sharedManager].dataBase deleteFromTable:[NITEvent tableDB] where:where];
}

- (NSString*)whereConditionORForEvents:(NSArray<NITEvent*>*)events
{
    return [[events bk_map:^id(NITEvent *obj) {
        return [NSString stringWithFormat:@"\"%@\" = %lu", @keypath1(obj.index), (unsigned long)obj.index];
    }] componentsJoinedByString:@" OR "];
}

@end
