//
//  NITStatisticManager+Private.h
//  NITStatistic
//
//  Created by Алексей Гуляев on 08.09.16.
//  Copyright © 2016 Napoleon It. All rights reserved.
//

#include "NITStatisticManager.h"

@class CLLocation;

@interface NITStatisticManager (Private)

+ (void)setLocation:(CLLocation*)location;
+ (void)sessionStart:(NSDictionary*)data;
+ (void)sessionEnd:(NSDictionary*)data;

@end
