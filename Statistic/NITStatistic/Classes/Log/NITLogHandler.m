//
//  NITLogHandler.m
//  NITStatistic
//
//  Created by Алексей Гуляев on 07.09.16.
//  Copyright © 2016 Napoleon It. All rights reserved.
//

#import "NITLogHandler.h"
#import "NITLogObserver.h"
#import "NITLogOperationSender.h"
#import <UIKit/UIKit.h>

static NSTimeInterval const NITTimerStartInterval = 10.f;
static NSTimeInterval const NITTimerInterval = 60.f;

@interface NITLogHandler()
{
    NSOperationQueue *_queueWrite;
    NSOperationQueue *_queueRead;
    
    NSTimer *_timer;
    
    NITLogObserver *_observer;
}

@property(nonatomic) NSTimeInterval intervalTimer;
@property(nonatomic, readonly) NSOperationQueue *queueRead;

@end

@implementation NITLogHandler

#pragma mark - Lifecycle

- (instancetype)init
{
    NSAssert(NO, @"Using sharedManager");
    return nil;
}

- (instancetype)initPrivate
{
    self = [super init];
    
    if (self)
    {
        _observer = [NITLogObserver new];
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(applicationDidBecomeActiveNotification:)
                                                     name:UIApplicationDidBecomeActiveNotification
                                                   object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(applicationDidEnterBackgroundNotification:)
                                                     name:UIApplicationDidEnterBackgroundNotification
                                                   object:nil];
    }
    
    return self;
}

+ (instancetype)sharedManager
{
    static dispatch_once_t once;
    static id sharedInstance;
    dispatch_once(&once, ^{
        sharedInstance = [[self alloc] initPrivate];
    });
    return sharedInstance;
}

#pragma mark - Custom Accessors

- (NSOperationQueue *)queueWrite
{
    static dispatch_once_t onceTokenQueueWrite;
    
    dispatch_once(&onceTokenQueueWrite, ^{
        _queueWrite = [NSOperationQueue new];
        _queueWrite.maxConcurrentOperationCount = 1;
        _queueWrite.qualityOfService = NSQualityOfServiceUtility;
    });
    
    return _queueWrite;
}

- (NITLogObserver *)observer
{
    return _observer;
}

- (NSOperationQueue *)queueRead
{
    static dispatch_once_t onceTokenQueueRead;
    
    dispatch_once(&onceTokenQueueRead, ^{
        _queueRead = [NSOperationQueue new];
        _queueRead.maxConcurrentOperationCount = 1;
        _queueRead.qualityOfService = NSQualityOfServiceUtility;
    });
    
    return _queueRead;
}

#pragma mark - Public

#pragma mark - Notification

- (void)applicationDidBecomeActiveNotification:(NSNotification*)notification
{
    self.intervalTimer = NITTimerStartInterval;
    [self startTimerRepeats:NO];
}

- (void)applicationDidEnterBackgroundNotification:(NSNotification*)notification
{
    [self endTimer];
    [self.queueRead cancelAllOperations];
}

#pragma mark - Private

- (void)startTimerRepeats:(BOOL)yesOrNo
{
    _timer = [NSTimer scheduledTimerWithTimeInterval:self.intervalTimer
                                              target:self
                                            selector:@selector(tickTimer:)
                                            userInfo:@{@"repeats":@(yesOrNo)}
                                             repeats:yesOrNo];
}

- (void)endTimer
{
    [_timer invalidate];
    _timer = nil;
}

- (void)tickTimer:(NSTimer*)timer
{
    [self.queueRead addOperation:[NITLogOperationSender new]];
    
    NSDictionary *userData = timer.userInfo;
    NSNumber *repeats = userData[@"repeats"];
    if ([repeats isKindOfClass:[NSNumber class]] && [repeats boolValue] == NO)
    {
        [self endTimer];
        self.intervalTimer = NITTimerInterval;
        [self startTimerRepeats:YES];
    }
}

@end
