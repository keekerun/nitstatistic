//
//  NITStatisticManager.h
//  NITStatistic
//
//  Created by Алексей Гуляев on 30.08.16.
//  Copyright © 2016 Napoleon It. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NITStatisticManager : NSObject

/**
 *  Инициализирует работу системы статистики
 *
 *  @param urlServer URL сервера сбора статистики
 */
+ (void)startedWithURLServer:(nonnull NSString*)urlServer;

/**
 *  Сообщает о случившемся событии
 *
 *  @param eventKey строковый ключ события
 */
+ (void)addEvent:(nonnull NSString *)eventKey;


/**
 *  Сообщает о случившемся событии
 *
 *  @param eventKey строковый ключ события
 *  @param data     дополнительные данные события
 */
+ (void)addEvent:(nonnull NSString *)eventKey data:(nullable NSDictionary<NSString*, NSObject*>*)data;


/**
 *  Устанавливает идентификатор пользователя для связи событий с конкретным аккаунтом
 *
 *  @param userId Строковый идентификатор пользователя
 */
+ (void)setUserId:(nullable NSString *)userId;

@end
