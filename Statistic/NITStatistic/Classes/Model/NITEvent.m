//
//  NITEvent.m
//  NITStatistic
//
//  Created by Алексей Гуляев on 05.09.16.
//  Copyright © 2016 Napoleon It. All rights reserved.
//

#import "NITEvent.h"
#import "DataBase.h"

@implementation NITEvent

#pragma mark - Lifecycle

- (instancetype)initWithAttribute:(NSDictionary*)dictionary database:(DataBase *)database
{
    self = [super init];
    
    if (self)
    {
        self.database = database;
        
        self.index = [[dictionary objectForKey:@keypath1(self.index)] unsignedIntegerValue];
        self.status = [[dictionary objectForKey:@keypath1(self.index)] unsignedIntegerValue];
        self.method = [[dictionary objectForKey:@keypath1(self.method)] unsignedIntegerValue];
        self.timestamp = [[dictionary objectForKey:@keypath1(self.timestamp)] doubleValue];
        
        NSString *device = [dictionary objectForKey:@keypath1(self.device)];
        if ([device isKindOfClass:[NSString class]])
        {
            self.device = device;
        }
        
        NSString *event = [dictionary objectForKey:@keypath1(self.event)];
        if ([event isKindOfClass:[NSString class]])
        {
            self.event = event;
        }
        
        id data = [dictionary objectForKey:@keypath1(self.data)];
        
        if ([data isKindOfClass:[NSDictionary class]])
        {
            self.data = data;
        }
        else if ([data isKindOfClass:[NSString class]])
        {
            self.dataJSON = data;
        }
    }
    
    return self;
}

- (NSDictionary*)dictionaryWithOptions:(NITEventGenerateDictionaryOptions)options
{
    NSMutableDictionary *dictionary = [NSMutableDictionary new];
    
    SET_VALUE_IS_NOT_NIL(@(self.index), dictionary, @keypath1(self.index));
    SET_VALUE_IS_NOT_NIL(@(self.status), dictionary, @keypath1(self.status));
    SET_VALUE_IS_NOT_NIL(self.event, dictionary, @keypath1(self.event));
    
    if (options & NITEventGenerateDictionaryEventMethodString)
    {
        NSString *methodString = [NITEvent stringEventMethod:self.method];
        SET_VALUE_IS_NOT_NIL(methodString, dictionary, @keypath1(self.method));
    }
    else
    {
        SET_VALUE_IS_NOT_NIL(@(self.method), dictionary, @keypath1(self.method));
    }
    
    SET_VALUE_IS_NOT_NIL(@(self.timestamp), dictionary, @keypath1(self.timestamp));
    SET_VALUE_IS_NOT_NIL(self.device, dictionary, @keypath1(self.device));
    
    if (options & NITEventGenerateDictionaryDataJSONString)
    {
        NSString *json = self.dataJSON;
        SET_VALUE_IS_NOT_NIL(json, dictionary, @keypath1(self.data));
    }
    else
    {
        SET_VALUE_IS_NOT_NIL(self.data, dictionary, @keypath1(self.data));
    }
    
    return [NSDictionary dictionaryWithDictionary:dictionary];
}


#pragma mark - Custom Accessors

- (void)setData:(NSDictionary *)data
{
    _data = data;
    
    if ([_data isKindOfClass:[NSDictionary class]])
    {
        NSError *error;
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:data
                                                           options:0
                                                             error:&error];
        
        if (error)
        {
            NSLog(@"Error JSONObjectWithData: %@", error.description);
        }
        
        NSString *jsonString = [[NSString alloc] initWithData:jsonData
                                                     encoding:NSUTF8StringEncoding];
        
        _dataJSON = jsonString ?: nil;
    }
    else
    {
        _dataJSON = nil;
    }
}

- (void)setDataJSON:(NSString *)dataJSON
{
    _dataJSON = dataJSON;
    
    if ([_dataJSON isKindOfClass:[NSString class]])
    {
        NSError *error;
        NSDictionary *jsonResponse = [NSJSONSerialization JSONObjectWithData:[_dataJSON dataUsingEncoding:NSUTF8StringEncoding]
                                                                     options:kNilOptions
                                                                       error:&error];
        
        if (error)
        {
            NSLog(@"Error JSONObjectWithData: %@", error.description);
        }
        
        _data = jsonResponse ?: nil;
    }
    else
    {
        _data = nil;
    }
}

#pragma mark - Public

+ (NSString*)tableDB
{
    return NSStringFromClass([NITEvent class]);
}

+ (void)createTableWithDatabase:(DataBase*)database
{
    __unused NITEvent *_e;
    
    NSString *createEventSQL = [NSString stringWithFormat:@"CREATE TABLE IF NOT EXISTS \"%@\"", [self tableDB]];
    
    NSMutableArray *columns = [NSMutableArray new];
    
    [columns addObject:[NSString stringWithFormat:@"\"%@\" INTEGER PRIMARY KEY  AUTOINCREMENT  NOT NULL  UNIQUE", @keypath1(_e.index)]];
    [columns addObject:[NSString stringWithFormat:@"\"%@\" INTEGER", @keypath1(_e.status)]];
    [columns addObject:[NSString stringWithFormat:@"\"%@\" INTEGER", @keypath1(_e.method)]];
    [columns addObject:[NSString stringWithFormat:@"\"%@\" TEXT", @keypath1(_e.event)]];
    [columns addObject:[NSString stringWithFormat:@"\"%@\" DOUBLE", @keypath1(_e.timestamp)]];
    [columns addObject:[NSString stringWithFormat:@"\"%@\" TEXT", @keypath1(_e.device)]];
    [columns addObject:[NSString stringWithFormat:@"\"%@\" TEXT", @keypath1(_e.data)]];
    
    createEventSQL = [createEventSQL stringByAppendingFormat:@" (%@);", [columns componentsJoinedByString:@" , "]];
    
    [database queryString:createEventSQL];
}

- (void)saveDB
{
    NSMutableDictionary *dic = [self dictionaryWithOptions:NITEventGenerateDictionaryDataJSONString].mutableCopy;
 
    if (self.index == 0)
    {
        [dic removeObjectForKey:@keypath1(self.index)];
        [self.database insertToTable:NSStringFromClass([NITEvent class]) data:dic];
    }
    else
    {
        NSString *where = [NSString stringWithFormat:@"'%@' = %lu", @keypath1(self.index), (unsigned long)self.index];
        [self.database updateTable:NSStringFromClass([NITEvent class]) data:dic where:where];
    }
    
}

- (void)removeDB
{
    if (self.index > 0)
    {
        NSString *where = [NSString stringWithFormat:@"'%@' = %lu", @keypath1(self.index), (unsigned long)self.index];
        [self.database deleteFromTable:NSStringFromClass([NITEvent class]) where:where];
    }
}

#pragma mark - Private

+ (NSString*)stringEventMethod:(NITEventMethod)method
{
    switch (method)
    {
        case NITEventMethodSessionStart:
            return @"sessionStart";
            break;
            
        case NITEventMethodSessionEnd:
            return @"sessionEnd";
            break;
            
        case NITEventMethodEvent:
            return @"event";
            break;
            
        case NITEventMethodSetUserId:
            return @"setUserId";
            break;
            
        case NITEventMethodSetLocation:
            return @"setLocation";
            break;
    }
}

@end
