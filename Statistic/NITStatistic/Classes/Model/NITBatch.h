//
//  NITBatch.h
//  NITStatistic
//
//  Created by Алексей Гуляев on 05.09.16.
//  Copyright © 2016 Napoleon It. All rights reserved.
//

#import <Foundation/Foundation.h>

@class NITEvent;

@interface NITBatch : NSObject

@property(nonatomic,strong) NSString *command;
@property(nonatomic,strong) NSArray<NITEvent*> *batchs;

- (instancetype)initWithEvents:(NSArray<NITEvent*>*)events;

- (NSDictionary*)dictionary;

@end
