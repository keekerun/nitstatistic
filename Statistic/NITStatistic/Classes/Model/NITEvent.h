//
//  NITEvent.h
//  NITStatistic
//
//  Created by Алексей Гуляев on 05.09.16.
//  Copyright © 2016 Napoleon It. All rights reserved.
//

#import <Foundation/Foundation.h>

@class DataBase;

typedef NS_ENUM(NSUInteger, NITEventMethod) {
    NITEventMethodSessionStart  = 1,
    NITEventMethodSessionEnd    = 2,
    NITEventMethodEvent         = 3,
    NITEventMethodSetLocation   = 4,
    NITEventMethodSetUserId     = 5
};

typedef NS_ENUM(NSUInteger, NITEventStatus) {
    NITEventStatusNew = 0,
    NITEventStatusSending = 1,
    NITEventStatusError = 2
};


typedef NS_OPTIONS(NSUInteger, NITEventGenerateDictionaryOptions) {
    NITEventGenerateDictionaryDataJSONString = (1UL << 0),
    NITEventGenerateDictionaryEventMethodString = (1UL << 1)
};

@interface NITEvent : NSObject

@property(nonatomic) NSUInteger index;
@property(nonatomic) NITEventStatus status;
@property(nonatomic) NITEventMethod method;
@property(nonatomic,strong) NSString *event;
@property(nonatomic) NSTimeInterval timestamp;
@property(nonatomic,strong) NSString *device;
@property(nonatomic,strong) NSDictionary *data;
@property(nonatomic,strong) NSString *dataJSON;

@property(nonatomic,strong) DataBase *database;

- (instancetype)initWithAttribute:(NSDictionary*)dictionary database:(DataBase*)database;
- (NSDictionary*)dictionaryWithOptions:(NITEventGenerateDictionaryOptions)options;

+ (NSString*)tableDB;
+ (void)createTableWithDatabase:(DataBase*)database;
- (void)saveDB;
- (void)removeDB;

+ (NSString*)stringEventMethod:(NITEventMethod)method;

@end
