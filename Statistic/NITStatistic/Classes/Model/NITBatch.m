//
//  NITBatch.m
//  NITStatistic
//
//  Created by Алексей Гуляев on 05.09.16.
//  Copyright © 2016 Napoleon It. All rights reserved.
//

#import "NITBatch.h"
#import "NITEvent.h"
#import "NSArray+BlocksKit.h"

@implementation NITBatch

#pragma mark - Lifecycle

- (instancetype)init
{
    self = [super init];
    
    if (self)
    {
        self.command = @"batchMethod";
    }

    return self;
}

- (instancetype)initWithEvents:(NSArray<NITEvent*>*)events
{
    self = [self init];
    
    if (self)
    {
        self.batchs = events;
    }
    
    return self;
}

- (NSDictionary*)dictionary
{
    NSMutableDictionary *dictionary = [NSMutableDictionary new];
    
    SET_VALUE_IS_NOT_NIL(self.command, dictionary, @keypath1(self.command));
    
    NSArray *batchs = [self.batchs bk_mapNotNil:^id(NITEvent *obj) {
        return [obj dictionaryWithOptions: NITEventGenerateDictionaryEventMethodString];
    }];
    
    SET_VALUE_IS_NOT_NIL(batchs, dictionary, @keypath1(self.batchs));
    
    return [NSDictionary dictionaryWithDictionary:dictionary];
}


#pragma mark - Custom Accessors

#pragma mark - Public

#pragma mark - Private

@end
