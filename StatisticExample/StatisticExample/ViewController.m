//
//  ViewController.m
//  StatisticExample
//
//  Created by Алексей Гуляев on 09.09.16.
//  Copyright © 2016 Napoleon It. All rights reserved.
//

#import "ViewController.h"
#import <CoreLocation/CoreLocation.h>
#import <NITStatistic/NITStatisticManager.h>

@interface ViewController () <CLLocationManagerDelegate>
{
    CLLocationManager *_locationManager;
}

@property(nonatomic,readonly) CLLocationManager *locationManager;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
 
    [NITStatisticManager setUserId:@"9023h2304h23084h230842"];
    
    [self startMonitoringLocation];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [NITStatisticManager addEvent:@"viewWillAppear controller" data:@{@"screenClass": NSStringFromClass([self class])}];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [NITStatisticManager addEvent:@"viewDidAppear controller" data:@{@"screenClass": NSStringFromClass([self class])}];
}

- (CLLocationManager *)locationManager
{
    if (!_locationManager)
    {
        @synchronized (self)
        {
            if (!_locationManager)
            {
                _locationManager = [CLLocationManager new];
                _locationManager.delegate = self;
                _locationManager.activityType = CLActivityTypeAutomotiveNavigation;
                _locationManager.desiredAccuracy = kCLLocationAccuracyBestForNavigation;
                //_locationManager.distanceFilter = keDistanceFilter;
            }
        }
    }
    
    return _locationManager;
}

- (void)startMonitoringLocation
{
    if ([CLLocationManager locationServicesEnabled])
    {
        if ([self.locationManager respondsToSelector:@selector(requestAlwaysAuthorization)])
        {
            [self.locationManager requestWhenInUseAuthorization];
        }
        
        [self.locationManager startUpdatingLocation];
    }
}

- (void)stopMonitoringLocation
{
    [self.locationManager stopUpdatingLocation];
}

#pragma mark - CLLocationManagerDelegate

- (void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status
{
    switch (status)
    {
        case kCLAuthorizationStatusDenied:
            // Need show request
            break;
            
        case kCLAuthorizationStatusRestricted:
            
            break;
            
        case kCLAuthorizationStatusNotDetermined:
            
            break;
            
        case kCLAuthorizationStatusAuthorizedAlways:
            
            break;
            
        case kCLAuthorizationStatusAuthorizedWhenInUse:
            
            break;
            
        default:
            break;
    }
}

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray<CLLocation *> *)locations
{
    
}

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    
}

@end
