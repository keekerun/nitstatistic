//
//  NITStatisticManagerExtended.m
//  StatisticExample
//
//  Created by Алексей Гуляев on 05.10.16.
//  Copyright © 2016 Napoleon It. All rights reserved.
//

#import "NITStatisticManagerExtended.h"

@implementation NITStatisticManagerExtended

+ (void)addOrderToCard:(NSString*)cardId item:(NSString*)itemId
{
    NSDictionary *data  = @{
                            @"cardId":cardId,
                            @"itemId":itemId
                            };
    
    [self addEvent:@"StatisticKeyAddOrder" data:data];
}

@end
