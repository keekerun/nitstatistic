//
//  NITStatisticManagerExtended.h
//  StatisticExample
//
//  Created by Алексей Гуляев on 05.10.16.
//  Copyright © 2016 Napoleon It. All rights reserved.
//

#import <NITStatistic/NITStatistic.h>

@interface NITStatisticManagerExtended : NITStatisticManager

+ (void)addOrderToCard:(NSString*)cardId item:(NSString*)itemId;
@end
