//
//  AppDelegate.h
//  StatisticExample
//
//  Created by Алексей Гуляев on 09.09.16.
//  Copyright © 2016 Napoleon It. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

