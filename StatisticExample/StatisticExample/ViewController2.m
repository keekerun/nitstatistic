//
//  ViewController2.m
//  StatisticExample
//
//  Created by Алексей Гуляев on 28.09.16.
//  Copyright © 2016 Napoleon It. All rights reserved.
//

#import "ViewController2.h"
#import <NITStatistic/NITStatisticManager.h>

@interface ViewController2 ()

@end

@implementation ViewController2

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [NITStatisticManager setUserId:@"9023h2304h23084h230842"];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [NITStatisticManager addEvent:@"viewWillAppear controller2" data:@{@"screenClass": NSStringFromClass([self class])}];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [NITStatisticManager addEvent:@"viewDidAppear controller2" data:@{@"screenClass": NSStringFromClass([self class])}];
}

@end
